package com.example.nejira.knjige17832;


import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class DodavanjeKnjigeFragment extends Fragment {

    public DodavanjeKnjigeFragment() {
        // Required empty public constructor
    }

    ImageView naslovnaStrana;
    private String slika;

    iDodavanjeKnjigeFragment iDodavanjeKnjigeFragment;

    BazaOpenHelper db;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dodavanje_knjige, container, false);

        db = new BazaOpenHelper(getContext());

        final Button dNadjiSliku=(Button)view.findViewById(R.id.dNadjiSliku);
        final Button dUpisiKnjigu=(Button)view.findViewById(R.id.dUpisiKnjigu);
        final Button dPonisti=(Button)view.findViewById(R.id.dPonisti);
        final Spinner sKategorijaKnjige=(Spinner)view.findViewById(R.id.sKategorijaKnjige);
        final EditText nazivKnjige=(EditText)view.findViewById(R.id.nazivKnjige);
        final EditText imeAutora=(EditText)view.findViewById(R.id.imeAutora);
        naslovnaStrana=(ImageView)view.findViewById(R.id.naslovnaStr);

        iDodavanjeKnjigeFragment = (iDodavanjeKnjigeFragment)getActivity();

        //postavljanje spinnera
        final ArrayList<String> kategorije;
        //kategorije= (ArrayList<String>) getArguments().getStringArrayList("kategorije");
        kategorije = db.dajKategorije();
        final ArrayAdapter<String> adapterSpinner=new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, kategorije);
        sKategorijaKnjige.setAdapter(adapterSpinner);

        dPonisti.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                iDodavanjeKnjigeFragment.ponisti();
            }
        });

        dUpisiKnjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!nazivKnjige.getText().toString().isEmpty() && !imeAutora.getText().toString().isEmpty() && sKategorijaKnjige.getCount() != 0) {
                    Intent data = new Intent();
                    data.putExtra("knjiga",(Serializable) new Knjiga(nazivKnjige.getText().toString(), imeAutora.getText().toString(), kategorije.get(sKategorijaKnjige.getSelectedItemPosition()), slika));
                    iDodavanjeKnjigeFragment.upisiKnjigu(data);
                }
            }
        });

        dNadjiSliku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent=new Intent();
                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.KITKAT){
                    myIntent.setAction(Intent.ACTION_OPEN_DOCUMENT);
                    myIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    myIntent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
                }
                else{
                    myIntent.setAction(Intent.ACTION_GET_CONTENT);
                }
                myIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                myIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                myIntent.setType("image/*");
                startActivityForResult(Intent.createChooser(myIntent, "Select image"), 1);
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try{
            Uri uri=data.getData();
            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.KITKAT){
                final int takeFlags=data.getFlags() & Intent.FLAG_GRANT_READ_URI_PERMISSION;
                ContentResolver contentResolver=getActivity().getApplicationContext().getContentResolver();
                contentResolver.takePersistableUriPermission(uri, takeFlags);
                try{
                    Bitmap bitmapa= MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                    naslovnaStrana.setImageBitmap(bitmapa);
                } catch (IOException e){
                    e.printStackTrace();
                }
                slika=uri.toString();
            }
        }catch (Exception e){
            Log.d("poruka", e.toString());
        }
    }
}
