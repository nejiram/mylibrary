package com.example.nejira.knjige17832;

import java.io.Serializable;
import java.util.ArrayList;

public class Autor implements Serializable{

    //novi atributi:
    private String imeiPrezime;
    private ArrayList<String> knjige;

    //stari atributi:
    private String imeAutora;
    private int brojKnjiga = 0;

    public Autor(){}

    public String getImeAutora() {
        return imeAutora;
    }

    public void setImeAutora(String imeAutora) {
        this.imeAutora = imeAutora;
    }

    public Autor(String imeAutora, int brojKnjiga) {
        this.imeAutora = imeAutora;
        this.brojKnjiga = brojKnjiga;
    }

    public Autor(String a){
        this.imeiPrezime=a;
    }

    public String getImeiPrezime() {
        return imeiPrezime;
    }

    public void setImeiPrezime(String imeiPrezime) {
        this.imeiPrezime = imeiPrezime;
    }

    public ArrayList<String> getKnjige() {
        return knjige;
    }

    public void setKnjige(ArrayList<String> knjige) {
        this.knjige = knjige;
    }


    public Autor(String imeiPrezime, String id){
        this.imeiPrezime=imeiPrezime;
        dodajKnjigu(id);
    }

    public int getBrojKnjiga() {
        return brojKnjiga;
    }

    public void setBrojKnjiga(int brojKnjiga) {
        this.brojKnjiga = brojKnjiga;
    }

    private  void  dodajKnjigu(String id){
        boolean postojiId=false;
        knjige = new ArrayList<>();
        if (knjige.size() > 0)
        for (int i=0; i<knjige.size(); i++){
            if (knjige.get(i).equals(id)){
                postojiId=true;
                break;
            }
        }
        if(!postojiId){
            knjige.add(id);
        }
    }

}
