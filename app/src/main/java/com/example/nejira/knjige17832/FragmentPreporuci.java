package com.example.nejira.knjige17832;


import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;
import static android.support.v4.content.PermissionChecker.checkSelfPermission;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentPreporuci extends Fragment {


    public FragmentPreporuci() {
        // Required empty public constructor
    }

    BazaOpenHelper db;

    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_preporuci, container, false);

        db = new BazaOpenHelper(getContext());

        final TextView nazivKnjige=view.findViewById(R.id.pNaziv);
        final TextView nazivAutora=view.findViewById(R.id.pAutor);
        final TextView opis=view.findViewById(R.id.pOpis);
        final TextView datumObjavljivanja=view.findViewById(R.id.pDatumObjavljivanja);
        final  TextView brojStranica=view.findViewById(R.id.pBrojStranica);
        final ImageView naslovnaStrana=view.findViewById(R.id.pNaslovna);
        final Button posalji=view.findViewById(R.id.dPosalji);
        final Spinner sKontakti=view.findViewById(R.id.sKontakti);


        final Knjiga knjiga=getArguments().getParcelable("knjiga");


        if(!knjiga.isJeLiNova()){
            nazivKnjige.setText(knjiga.getNazivKnjige());
            nazivAutora.setText(knjiga.getPisac());
            if(knjiga.getSlikaUrl()!=null){
                try{
                    Bitmap bitmapa= MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), Uri.parse(knjiga.getSlikaUrl()));
                    naslovnaStrana.setImageBitmap(bitmapa);
                }
                catch(IOException e){
                    e.printStackTrace();
                }
            }

        }
        else if(knjiga.isJeLiNova()){
            if(knjiga.getNaziv()!=null) nazivKnjige.setText(knjiga.getNaziv());
            else nazivKnjige.setText("Naziv knjige");
            if(knjiga.getAutori().get(0).getImeiPrezime()!=null) nazivAutora.setText(knjiga.getAutori().get(0).getImeiPrezime());
            else nazivAutora.setText("Autor");
            if(knjiga.getDatumObjavljivanja()!=null) datumObjavljivanja.setText(knjiga.getDatumObjavljivanja());
            else datumObjavljivanja.setText("Datum objavljivanja");
            if(knjiga.getOpis()!=null) opis.setText(knjiga.getOpis());
            else opis.setText("Opis");
            if(String.valueOf(knjiga.getBrojStranica())!=null) brojStranica.setText(( String.valueOf(knjiga.getBrojStranica())));
            else brojStranica.setText("Broj stranica");
            if(knjiga.getSlika()!=null) Picasso.with(getContext()).load(knjiga.getSlika().toString()).into(naslovnaStrana);
            else naslovnaStrana.setImageResource(R.drawable.ic_launcher_background);
        }

        final ArrayList<String> kontakti=new ArrayList<>();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(getContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
        } else {

            Cursor c = getContext().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.Contacts.DISPLAY_NAME);

            if(c.getCount()>0){
                String contactName = "";
                String mail="";
               while(c.moveToNext()){
                   String id=c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
                   Log.d("ID", id+"");
                   Cursor emailC=getContext().getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{id}, null);
                   while(emailC.moveToNext()){
                       contactName=c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                       mail=emailC.getString(emailC.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                       kontakti.add(contactName + ":" + mail);
                   }
                   emailC.close();
               }
                c.close();
            }

            final ArrayAdapter<String> adapterSpinnerKontakti = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, kontakti);
            sKontakti.setAdapter(adapterSpinnerKontakti);
        }



        posalji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String pom=kontakti.get(sKontakti.getSelectedItemPosition());
                String[] parts=pom.split(":");
                String[] TO={parts[1]};
                Intent emailIntent=new Intent(Intent.ACTION_SEND);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, TO );
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Preporuka");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "Zdravo "+ parts[0] + ", \n"+"Pročitaj knjigu "+ nazivKnjige.getText().toString() +" od autora "+ nazivAutora.getText().toString() +"!");

                PackageManager packageManager = getActivity().getPackageManager();
                List<ResolveInfo> activities = packageManager.queryIntentActivities(emailIntent, PackageManager.MATCH_DEFAULT_ONLY);
                if(activities.size()>0){
                    try{
                        startActivity(Intent.createChooser(emailIntent, "Send mail"));
                        getActivity().finish();
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }
                }
                else {
                    Toast.makeText(getContext(), "Nema dotupnih aplikacija za ovu akciju!", Toast.LENGTH_SHORT).show();
                }


            }
        });

        return view;
    }


}


