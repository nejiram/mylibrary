package com.example.nejira.knjige17832;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Nejira on 30.03.2018..
 */

public class Knjiga implements Serializable, Parcelable {

    //novi atributi:
    private String id;
    private String naziv;
    private ArrayList<Autor> autori;
    private String opis;
    private String datumObjavljivanja;
    private URL slika;
    private int brojStranica;

    private boolean jeLiNova;
    //private String slika;

    //stari atributi:
    private String nazivKnjige;
    private String pisac;
    private String kategorija;
    private String slikaUrl;
    private boolean obojen;

    public Knjiga(){}

    public Knjiga(String nazivKnjige, String pisac, String kategorija, String slikaUrl)  {
        this.nazivKnjige=nazivKnjige;
        this.pisac=pisac;
        this.kategorija=kategorija;
        this.slikaUrl=slikaUrl;
        this.obojen = false;
        jeLiNova=false;
        this.naziv=nazivKnjige;
        //this.autori.add(new Autor(pisac));
        this.opis="Opis";
        this.datumObjavljivanja="Datum objavljivanja";
        //URL url=new URL(slikaUrl);
        //this.slika=url;
        this.brojStranica=0;
    }

    public Knjiga(String nazivKnjige, String pisac, String kategorija, String slikaUrl, boolean obojen)  {
        this.nazivKnjige=nazivKnjige;
        this.pisac=pisac;
        this.kategorija=kategorija;
        this.slikaUrl=slikaUrl;
        this.obojen = obojen;
        jeLiNova=false;
        this.naziv=nazivKnjige;
        //this.autori.add(new Autor(pisac));
        this.opis="Opis";
        this.datumObjavljivanja="Datum objavljivanja";
        //URL url=new URL(slikaUrl);
        //this.slika=url;
        this.brojStranica=0;
    }

    public Knjiga(String id, String kategorija){
        this.id=id;
        this.kategorija=kategorija;
    }


    protected Knjiga(Parcel in) {
        id = in.readString();
        naziv = in.readString();
        opis = in.readString();
        datumObjavljivanja = in.readString();
        brojStranica = in.readInt();
        jeLiNova = in.readByte() != 0;
        nazivKnjige = in.readString();
        pisac = in.readString();
        kategorija = in.readString();
        slikaUrl = in.readString();
        obojen = in.readByte() != 0;
    }

    public static final Creator<Knjiga> CREATOR = new Creator<Knjiga>() {
        @Override
        public Knjiga createFromParcel(Parcel in) {
            return new Knjiga(in);
        }

        @Override
        public Knjiga[] newArray(int size) {
            return new Knjiga[size];
        }
    };

    public String getNazivKnjige() {
        return nazivKnjige;
    }

    public void setNazivKnjige(String nazivKnjige) {
        this.nazivKnjige = nazivKnjige;
    }

    public String getPisac() {
        return pisac;
    }

    public void setPisac(String pisac) {
        this.pisac = pisac;
    }

    public String getKategorija() {
        return kategorija;
    }

    public void setKategorija(String kategorija) {
        this.kategorija = kategorija;
    }


    public Knjiga(String id, String naziv, ArrayList<Autor> autori, String opis, String datumObjavljivanja, URL slika, int brojStranica) {
        this.id = id;
        this.naziv = naziv;
        this.autori = autori;
        this.opis = opis;
        this.datumObjavljivanja = datumObjavljivanja;
        this.slika = slika;
        this.brojStranica = brojStranica;
        this.obojen=false;
        jeLiNova=true;
        this.nazivKnjige=naziv;
        this.pisac=autori.get(0).getImeiPrezime();
        this.kategorija="Kategorija";
    }

    public Knjiga(String id, String naziv, ArrayList<Autor> autori, String opis, String datumObjavljivanja, URL slika, int brojStranica, String kategorija) {
        this.id = id;
        this.naziv = naziv;
        this.autori = autori;
        this.opis = opis;
        this.datumObjavljivanja = datumObjavljivanja;
        this.slika = slika;
        this.brojStranica = brojStranica;
        this.kategorija=kategorija;
        this.obojen=false;
        jeLiNova=true;
        this.nazivKnjige=naziv;
        this.pisac=autori.get(0).getImeiPrezime();
    }

    public Knjiga(String id, String naziv, ArrayList<Autor> autori, String opis, String datumObjavljivanja, URL slika, int brojStranica, String kategorija, boolean obojen) {
        this.id = id;
        this.naziv = naziv;
        this.autori = autori;
        this.opis = opis;
        this.datumObjavljivanja = datumObjavljivanja;
        this.slika = slika;
        this.brojStranica = brojStranica;
        this.kategorija=kategorija;
        this.obojen=obojen;
        jeLiNova=true;
        this.nazivKnjige=naziv;
        this.pisac=autori.get(0).getImeiPrezime();
    }

    public boolean isObojen() {
        return obojen;
    }

    public void setObojen(boolean obojen) {
        this.obojen = obojen;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public ArrayList<Autor> getAutori() {
        return autori;
    }

    public void setAutori(ArrayList<Autor> autori) {
        this.autori = autori;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getDatumObjavljivanja() {
        return datumObjavljivanja;
    }

    public void setDatumObjavljivanja(String datumObjavljivanja) {
        this.datumObjavljivanja = datumObjavljivanja;
    }

    public URL getSlika() { return slika; }

    public void setSlika(URL slika) {
        this.slika = slika;
    }

    public int getBrojStranica() {
        return brojStranica;
    }

    public void setBrojStranica(int brojStranica) {
        this.brojStranica = brojStranica;
    }

    public String getSlikaUrl() {
        return slikaUrl;
    }

    public void setSlikaUrl(String slikaUrl) {
        this.slikaUrl = slikaUrl;
    }

    public boolean isJeLiNova() {
        return jeLiNova;
    }

    public void setJeLiNova(boolean jeLiNova) {
        this.jeLiNova = jeLiNova;
    }


    public void dodajAutora(String imeAutora){
            autori = new ArrayList<>();
            autori.add(new Autor(imeAutora));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(naziv);
        dest.writeString(opis);
        dest.writeString(datumObjavljivanja);
        dest.writeInt(brojStranica);
        dest.writeByte((byte) (jeLiNova ? 1 : 0));
        dest.writeString(nazivKnjige);
        dest.writeString(pisac);
        dest.writeString(kategorija);
        dest.writeString(slikaUrl);
        dest.writeByte((byte) (obojen ? 1 : 0));
    }

    public Knjiga(Knjiga k){
        if(k.getId() == null){
            this.nazivKnjige = k.getNazivKnjige();
            this.pisac = k.getPisac();
            this.slikaUrl = k.getSlikaUrl();
            this.jeLiNova = k.isJeLiNova();
            this.obojen = k.isObojen();
            this.kategorija = k.getKategorija();
        } else{
            this.id = k.getId();
            this.naziv = k.getNaziv();
            this.autori = k.getAutori();
            this.slika = k.getSlika();
            this.opis = k.getOpis();
            this.datumObjavljivanja = k.getDatumObjavljivanja();
            this.brojStranica = k.getBrojStranica();
            this.jeLiNova = k.isJeLiNova();
            this.obojen = k.isObojen();
        }
    }
}
