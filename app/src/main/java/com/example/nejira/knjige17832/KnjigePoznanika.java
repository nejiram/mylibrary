package com.example.nejira.knjige17832;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class KnjigePoznanika extends IntentService {

    private String idKorisnika;
    private ResultReceiver receiver;
    public int STATUS_START=0;
    public int STATUS_FINISH=1;
    public int STATUS_ERROR=2;
    private ArrayList<Knjiga> listaKnjiga=new ArrayList<>();

    public KnjigePoznanika(){
        super(null);
    }

    public KnjigePoznanika(String name){
        super(name);
        //posao za konstruktor
    }

    @Override
    public void onCreate(){
        super.onCreate();
        //akcije koje se trebaju obaviti pri kreiranju servira
    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        idKorisnika=intent.getStringExtra("upit");
        receiver=intent.getParcelableExtra("receiver");
        Bundle bundle=new Bundle();
        receiver.send(STATUS_START, bundle.EMPTY);
        String query = null;

        try {
            query = URLEncoder.encode(idKorisnika, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        ArrayList<String> idevi=new ArrayList<>();

        try {
            String url1 = "https://www.googleapis.com/books/v1/users/"+query+"/bookshelves";
            URL url=new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String rezultat = convertStreamToString(in);
                JSONObject jo = new JSONObject(rezultat);
                JSONArray items = jo.getJSONArray("items");

                for (int i = 0; i < items.length(); i++) {
                    JSONObject book = items.getJSONObject(i);
                    if (book.has("id")) idevi.add(book.getString("id"));
                }
            }
            } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for(int k=0; k<idevi.size();k++) {
            String url1="https://www.googleapis.com/books/v1/users/"+query+"/bookshelves/" + idevi.get(k) + "/volumes";
            try {
                URL url = new URL(url1);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    String rezultat = convertStreamToString(in);
                    JSONObject jo = new JSONObject(rezultat);
                    JSONArray items = jo.getJSONArray("items");


                    String name = "", id = "", description = "", publishedDate = "", picture="";
                    int pages = 0;
                    ArrayList<Autor> listdata;
                    URL image;

                    for (int i = 0; i < items.length(); i++) {
                        JSONObject book = items.getJSONObject(i);

                        //JSONArray ids = book.getJSONArray("id");

                        if (book.has("id")) id = book.getString("id");
                        JSONObject vInfo = book.getJSONObject("volumeInfo");
                        if (vInfo.has("title")) name = vInfo.getString("title");

                        listdata = new ArrayList<Autor>();
                        if (vInfo.has("authors")) {
                            JSONArray authors = vInfo.getJSONArray("authors");
                            for (int j = 0; j < authors.length(); j++) {
                                if(authors.getString(j).equals("")) listdata.add(new Autor("Ime i prezime"));
                                else listdata.add(new Autor(authors.getString(j)));
                            }
                        }
                        else listdata.add(new Autor("Ime i prezime"));

                        if (vInfo.has("description")) description = vInfo.getString("description");
                        else description = "Opis";
                        //if(vInfo.has("thumbnail")) image = (URL) vInfo.get("thumbnail");
                        if(vInfo.has("imageLinks") && vInfo.getJSONObject("imageLinks").has("thumbnail")) {
                            picture = vInfo.getJSONObject("imageLinks").getString("thumbnail");
                            image=new URL(picture);
                        }
                        else image=null;
                        if (vInfo.has("publishedDate"))
                            publishedDate = vInfo.getString("publishedDate");
                        else publishedDate = "Datum objavljivanja";
                        if (vInfo.has("pageCount")) pages = vInfo.getInt("pageCount");
                        listaKnjiga.add(new Knjiga(id, name, listdata, description, publishedDate, image, pages));
                    }
                }
            } catch (IOException | JSONException e) {
                receiver.send(STATUS_ERROR, bundle.EMPTY);
                e.printStackTrace();
            }
        }
        bundle.putParcelableArrayList("listaKnjiga", listaKnjiga);
        receiver.send(STATUS_FINISH, bundle);

    }

    private String convertStreamToString(InputStream in) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                in.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }

}
