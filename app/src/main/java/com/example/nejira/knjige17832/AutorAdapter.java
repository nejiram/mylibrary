package com.example.nejira.knjige17832;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class AutorAdapter extends ArrayAdapter {

    Context context;
    int resource;

    public AutorAdapter(@NonNull Context context, int resource, @NonNull List objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View newView;
        if (convertView == null) {
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater layoutInflater = (LayoutInflater)getContext().getSystemService(inflater);
            layoutInflater.inflate(resource, (ViewGroup) newView, true);
        } else {
            newView = (LinearLayout)convertView;
        }
        Autor autor = (Autor) getItem(position);

        TextView itemImeAutora = newView.findViewById(R.id.itemImeAutora);
        TextView itemBrojKnjiga = newView.findViewById(R.id.itemBrojKnjiga);

        itemImeAutora.setText(autor.getImeAutora());
        itemBrojKnjiga.setText(Integer.toString(autor.getBrojKnjiga()));

        return newView;
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return super.getItem(position);
    }
}
