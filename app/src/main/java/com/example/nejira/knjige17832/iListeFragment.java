package com.example.nejira.knjige17832;

public interface iListeFragment {
    void dodavanjeKnjige();
    void onKategorijaClick(String pretraga, Boolean kategorija);
}
