package com.example.nejira.knjige17832;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class KnjigeFragment extends Fragment {

    iKnjigeFragment iKnjigeFragment;

    public KnjigeFragment() {
        // Required empty public constructor
    }


    BazaOpenHelper db;
    ArrayList<Knjiga> knjige;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_knjige, container, false);

        db = new BazaOpenHelper(getContext());

        final ListView lista = view.findViewById(R.id.listaKnjiga);
        final Button povratak = view.findViewById(R.id.dPovratak);

        iKnjigeFragment = (iKnjigeFragment)getActivity();

        //knjige = (ArrayList<Knjiga>) getArguments().getSerializable("knjige");
        knjige = db.dajKnjige();
        ArrayList<Knjiga> pokazne = new ArrayList<>();
        //pokazne.addAll(knjige);
        pokazne = db.dajKnjige();
        String kategorija = (String) getArguments().getString("kategorija");
        knjige = db.knjigeKategorije(db.dajIdKategorije(kategorija));
        Boolean daLiJeKategorija = getArguments().getBoolean("daLiJeKategorija");
        final ArrayList<Knjiga> obojene = new ArrayList<>();

        /*if(daLiJeKategorija) {
            for (int i = 0; i < pokazne.size(); i++) {
                if (!pokazne.get(i).getKategorija().equals(kategorija)) {
                    pokazne.remove(i);
                    i--;
                }
            }
        } else {
            /*for(int i = 0; i < pokazne.size(); i++){
                if(!pokazne.get(i).isJeLiNova() && !pokazne.get(i).getPisac().equals(kategorija)){
                    pokazne.remove(i);
                    i--;
                } else if(pokazne.get(i).isJeLiNova() && !pokazne.get(i).getAutori().get(0).equals(kategorija)){
                    pokazne.remove(i);
                    i--;
                }
            }*/
           /* pokazne = db.knjigeAutora(db.dajAutora(kategorija));
        }*/

           if(daLiJeKategorija){
               pokazne = db.knjigeKategorije(db.dajIdKategorije(kategorija));
           }
           else{
               pokazne = db.knjigeAutora(db.dajAutora(kategorija));
           }


        final CustomAdapter adapter = new CustomAdapter(getActivity(), R.layout.list_item_knjiga, pokazne);
        lista.setAdapter(adapter);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                //knjige.get(position).setObojen(true);
                knjige = db.dajKnjige();
                String knjigaZaObojit = "";
                if(knjige.get(position).isJeLiNova()) knjigaZaObojit = knjige.get(position).getNaziv();
                else knjigaZaObojit = knjige.get(position).getNazivKnjige();
                db.oboji(knjigaZaObojit, true);
                obojene.add(knjige.get(position));
                adapter.notifyDataSetChanged();
            }
        });



        povratak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent data = new Intent();
                //data.putExtra("obojene", obojene);
                iKnjigeFragment.povratak();
            }
        });
        return view;
    }
}
