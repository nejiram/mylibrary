package com.example.nejira.knjige17832;

import android.content.Intent;

public interface iDodavanjeKnjigeFragment {
    void ponisti();
    void upisiKnjigu(Intent data);
}
