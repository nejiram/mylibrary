package com.example.nejira.knjige17832;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentOnline extends Fragment implements DohvatiKnjige.IDohvatiKnjigeDone, DohvatiNajnovije.IDohvatiNajnovijeDone, MyResultReceiver.Receiver {

    ArrayList<Knjiga> knjige = new ArrayList<Knjiga>();
    ArrayList<String> pomocni;
    BazaOpenHelper db;

    View v;

    public FragmentOnline() {
        // Required empty public constructor
    }

    iFragmentOnline iFragmentOnline;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_online, container, false);
        db=new BazaOpenHelper(getActivity());

        final Spinner sKategorije=(Spinner)view.findViewById(R.id.sKategorije);
        final EditText upit=(EditText)view.findViewById(R.id.tekstUpit);
        final Spinner sRezultat=(Spinner)view.findViewById(R.id.sRezultat);
        final Button dRun=(Button)view.findViewById(R.id.dRun);
        final Button dAdd=(Button)view.findViewById(R.id.dAdd);
        final Button dPovratak=(Button)view.findViewById(R.id.dPovratak);

        iFragmentOnline = (iFragmentOnline)getActivity();

        //postavljanje spinnera sKategorije
        final ArrayList<String> kategorije;
        //kategorije= (ArrayList<String>) getArguments().getStringArrayList("kategorije");
        kategorije = db.dajKategorije();
        final ArrayAdapter<String> adapterSpinnerKategorije=new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, kategorije);
        sKategorije.setAdapter(adapterSpinnerKategorije);




        dPovratak.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                iFragmentOnline.povratakOnline();
            }
        });;


        dRun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!upit.getText().toString().contains(" ") && !upit.getText().toString().contains(";") && !upit.getText().toString().toLowerCase().contains("autor:") && !upit.getText().toString().toLowerCase().contains("korisnik:") ){
                    new DohvatiKnjige((DohvatiKnjige.IDohvatiKnjigeDone)FragmentOnline.this).execute(upit.getText().toString());
                }
                else if (upit.getText().toString().contains(";") && !upit.getText().toString().toLowerCase().contains("korisnik:") && !upit.getText().toString().toLowerCase().contains("autor:")){
                    new DohvatiKnjige((DohvatiKnjige.IDohvatiKnjigeDone)FragmentOnline.this).execute(upit.getText().toString().split(";"));
                }
                else if(upit.getText().toString().toLowerCase().contains("autor:") && !upit.getText().toString().toLowerCase().contains("korisnik:")){
                    String pom;
                    pom=upit.getText().toString().substring(6);
                    new DohvatiNajnovije((DohvatiNajnovije.IDohvatiNajnovijeDone)FragmentOnline.this).execute(pom);
                }
                else if(upit.getText().toString().toLowerCase().contains("korisnik:")){
                    String pom;
                    pom=upit.getText().toString().substring(9);
                    MyResultReceiver receiver=new MyResultReceiver(new Handler());
                    receiver.setmReceiver(FragmentOnline.this);
                    Intent intent=new Intent(Intent.ACTION_SYNC, null, getActivity(), KnjigePoznanika.class);
                    intent.putExtra("receiver", receiver);
                    intent.putExtra("upit", pom);
                    getActivity().startService(intent);
                }



            }
        });




        dAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ArrayList<Knjiga> dodavanjeKnjiga = (ArrayList<Knjiga>) getArguments().getSerializable("knjige");
                ArrayList<Knjiga> dodavanjeKnjiga = db.dajKnjige();
                //ArrayList<Autor> autors= (ArrayList<Autor>) getArguments().getSerializable("autori");
                ArrayList<Autor> autors = db.dajListuAutora();
                if(sRezultat.getSelectedItem()!=null && sKategorije.getSelectedItem()!=null){
                    for(int i=0; i<knjige.size();i++){
                            if(knjige.get(i).getNaziv().equals(sRezultat.getSelectedItem().toString())){
                            String slika="";
                            if (knjige.get(i).getSlika()!=null) slika=knjige.get(i).getSlika().toString();
                            //dodavanjeKnjiga.add(new Knjiga(knjige.get(i).getNaziv(), knjige.get(i).getAutori().get(0).getImeiPrezime(), sKategorije.getSelectedItem().toString(), slika));
                                //dodavanjeKnjiga.add(new Knjiga(knjige.get(i).getId(), knjige.get(i).getNaziv(), knjige.get(i).getAutori(), knjige.get(i).getOpis(), knjige.get(i).getDatumObjavljivanja(), knjige.get(i).getSlika(), knjige.get(i).getBrojStranica(), kategorije.get(sKategorije.getSelectedItemPosition())));

                                long id = -1;
                                try{
                                    Knjiga k = new Knjiga(knjige.get(i).getId(), knjige.get(i).getNaziv(), knjige.get(i).getAutori(), knjige.get(i).getOpis(), knjige.get(i).getDatumObjavljivanja(), knjige.get(i).getSlika(), knjige.get(i).getBrojStranica(), kategorije.get(sKategorije.getSelectedItemPosition()));
                                    id = db.dodajKnjigu(k);
                                }catch (MalformedURLException e){
                                    e.printStackTrace();
                                }

                                if(id != -1) {
                                    Toast.makeText(getContext(), "Knjiga je uspjesno dodana!", Toast.LENGTH_SHORT).show();
                                }
                                else if (id == -1){
                                    Toast.makeText(getContext(), "Greska!", Toast.LENGTH_SHORT).show();
                                }


                            //autori
                                for(int j=0;j<knjige.get(i).getAutori().size();j++){
                                    Autor autoriPomocni=new Autor(knjige.get(i).getAutori().get(j).getImeiPrezime(), 1);
                                    int brojac=0;
                                    if(autors.size()>0) {
                                        for (int k = 0; k < autors.size(); k++) {
                                            if (autors.get(k).getImeAutora()!=null && autors.get(k).getImeAutora().equals(autoriPomocni.getImeAutora())) {
                                                autors.get(k).setBrojKnjiga(autors.get(k).getBrojKnjiga() + 1);
                                                brojac++;
                                            }
                                        }
                                        if (brojac == 0) autors.add(autoriPomocni);
                                    }
                                }
                        }
                    }
                }
                iFragmentOnline.povratakOnline();
            }
        });

        v=view;
        return view;
    }


    @Override
    public void onDohvatiDone(ArrayList<Knjiga> rez) {
        knjige=rez;
        Spinner sRezultat=(Spinner)v.findViewById(R.id.sRezultat);
        pomocni=new ArrayList<String>();
        for(int i=0; i<knjige.size(); i++){
            pomocni.add(knjige.get(i).getNaziv());
        }
        final ArrayAdapter<String> adapterSpinnerRezultat=new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, pomocni);
        sRezultat.setAdapter(adapterSpinnerRezultat);
    }

    @Override
    public void onNajnovijeDone(ArrayList<Knjiga> rez) {
        knjige=rez;
        Spinner sRezultat=(Spinner)v.findViewById(R.id.sRezultat);
        pomocni=new ArrayList<String>();
        for(int i=0; i<knjige.size(); i++){
            pomocni.add(knjige.get(i).getNaziv());
        }
        final ArrayAdapter<String> adapterSpinnerRezultat=new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, pomocni);
        sRezultat.setAdapter(adapterSpinnerRezultat);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        if(resultCode==1){
            knjige=resultData.getParcelableArrayList("listaKnjiga");
            Spinner sRezultat=(Spinner)v.findViewById(R.id.sRezultat);
            pomocni=new ArrayList<String>();
            for(int i=0; i<knjige.size(); i++){
                pomocni.add(knjige.get(i).getNaziv());
            }
            final ArrayAdapter<String> adapterSpinnerRezultat=new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, pomocni);
            sRezultat.setAdapter(adapterSpinnerRezultat);
        }
    }
}
