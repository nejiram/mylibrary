package com.example.nejira.knjige17832;


import android.media.AudioRecord;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListeFragment extends Fragment {

    iListeFragment iListeFragment;
    iFragmentOnline iFragmentOnline;

    ArrayList<Autor> autors;
    BazaOpenHelper db;

    public ListeFragment() {
        // Required empty public constructor
    }

    Boolean kategorija;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_liste, container, false);
        db=new BazaOpenHelper(getActivity());

        final EditText unosTeksta=(EditText)view.findViewById(R.id.tekstPretraga);
        final Button pretraga=(Button)view.findViewById(R.id.dPretraga);
        final Button dodavanjeKnjige=(Button)view.findViewById(R.id.dDodajKnjigu);
        final Button dodavanjeKategorije=(Button)view.findViewById(R.id.dDodajKategoriju);
        final ListView listaKategorija=(ListView)view.findViewById(R.id.listaKategorija);
        final Button autor = view.findViewById(R.id.dAutori);
        final Button kategorije = view.findViewById(R.id.dKategorije);
        final Button dodajOnline=view.findViewById(R.id.dDodajOnline);

        kategorija = true;

        iListeFragment = (iListeFragment)getActivity();

        iFragmentOnline=(iFragmentOnline)getActivity();


        dodavanjeKategorije.setEnabled(false);

        //final ArrayList<String> unosi = getArguments().getStringArrayList("unosi");
        final ArrayList<String> unosi = db.dajKategorije();
        //autors = (ArrayList<Autor>) getArguments().getSerializable("autori");
        autors = db.dajListuAutora();

        final ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, unosi);
        listaKategorija.setAdapter(adapter);

        final AutorAdapter autorAdapter = new AutorAdapter(getActivity(), R.layout.list_item_autor, autors);

        autor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unosTeksta.setVisibility(View.GONE);
                pretraga.setVisibility(View.GONE);
                dodavanjeKategorije.setVisibility(View.GONE);
                listaKategorija.setAdapter(autorAdapter);
                kategorija = false;
            }
        });

        kategorije.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unosTeksta.setVisibility(View.VISIBLE);
                pretraga.setVisibility(View.VISIBLE);
                dodavanjeKategorije.setVisibility(View.VISIBLE);
                listaKategorija.setAdapter(adapter);
                kategorija = true;
            }
        });

        pretraga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String uneseniTekst = unosTeksta.getText().toString();
                adapter.getFilter().filter(uneseniTekst, new Filter.FilterListener() {
                    @Override
                    public void onFilterComplete(int count) {
                        if (adapter.isEmpty() && uneseniTekst.length()!=0 && uneseniTekst.trim().length()>0) {
                            dodavanjeKategorije.setEnabled(true);
                            dodavanjeKategorije.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view1) {
                                    db.dodajKategoriju(unosTeksta.getText().toString());
                                    unosi.add(0, unosTeksta.getText().toString());
                                    adapter.add(unosTeksta.getText().toString());
                                    adapter.notifyDataSetChanged();
                                    unosTeksta.setText("");
                                    adapter.getFilter().filter("");
                                    dodavanjeKategorije.setEnabled(false);
                                }
                            });
                        }
                    }
                });
            }
        });

        //dodavanje knjige intent
        dodavanjeKnjige.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                iListeFragment.dodavanjeKnjige();
            }
        });

        dodajOnline.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                iFragmentOnline.dodavanjeKnjigeOnline();
            }
        });

        listaKategorija.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String temp;
                if(kategorija){
                    temp = adapter.getItem(position).toString();
                } else {
                    temp = ((Autor) autorAdapter.getItem(position)).getImeAutora();
                }
                iListeFragment.onKategorijaClick(temp, kategorija);
            }
        });

        return view;
    }

}
