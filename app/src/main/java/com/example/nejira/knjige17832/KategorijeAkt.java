package com.example.nejira.knjige17832;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.Toast;

import com.facebook.stetho.Stetho;

import java.net.MalformedURLException;
import java.util.ArrayList;

public class KategorijeAkt extends AppCompatActivity implements iListeFragment, iKnjigeFragment, iDodavanjeKnjigeFragment, iFragmentOnline{

    ArrayList<Knjiga> knjige = new ArrayList<Knjiga>();
    ArrayList<String> unosi = new ArrayList<String>();
    ArrayList<Autor> autors = new ArrayList<>();

    BazaOpenHelper db;

    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Stetho.initializeWithDefaults(this);
        setContentView(R.layout.activity_kategorije_akt);

        db = new BazaOpenHelper(this);

        knjige = db.dajKnjige();
        autors = db.dajListuAutora();
        unosi = db.dajKategorije();

        fragmentManager = getSupportFragmentManager();
        //Bundle bundle = new Bundle();
        //unosi=db.dajKategorije();
        //bundle.putStringArrayList("unosi", unosi);
        //bundle.putSerializable("autori", autors);
        ListeFragment listeFragment = new ListeFragment();
        //listeFragment.setArguments(bundle);
        fragmentManager.beginTransaction().replace(R.id.frame1, listeFragment, "listeFragment").commit();
    }

    @Override
    public void dodavanjeKnjige() {
        //Bundle bundle = new Bundle();
        //bundle.putStringArrayList("kategorije", unosi);
        DodavanjeKnjigeFragment dodavanjeKnjigeFragment = new DodavanjeKnjigeFragment();
        //dodavanjeKnjigeFragment.setArguments(bundle);
        fragmentManager.beginTransaction().replace(R.id.frame1, dodavanjeKnjigeFragment, "dodavanjeKnjigeFragment").addToBackStack("dodavanjeKnjigeFragment").commit();
    }

    @Override
    public void onKategorijaClick(String pretraga, Boolean kategorija) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("knjige", knjige);
        bundle.putString("kategorija", pretraga);
        bundle.putBoolean("daLiJeKategorija", kategorija);
        KnjigeFragment knjigeFragment = new KnjigeFragment();
        knjigeFragment.setArguments(bundle);
        fragmentManager.beginTransaction().replace(R.id.frame1, knjigeFragment, "knjigeFragment").addToBackStack("knjigeFragment").commit();
    }

    @Override
    public void povratak() {
        /*ArrayList<Knjiga> obojene = (ArrayList<Knjiga>) data.getSerializableExtra("obojene");
        for(int i = 0; i<knjige.size(); i++){
            for(int j = 0; j<obojene.size(); j++){
                if(knjige.get(i).getNazivKnjige().equals(obojene.get(j).getNazivKnjige()))
                    knjige.get(i).setObojen(true);
            }
        }*/
        fragmentManager.popBackStack();
    }

    @Override
    public void ponisti() {
        fragmentManager.popBackStack();
    }

    @Override
    public void upisiKnjigu(Intent data) {
        Knjiga knjiga = (Knjiga) data.getSerializableExtra("knjiga");
        //knjige.add(knjiga);
        long id = -1;
        try {
            id = db.dodajKnjigu(knjiga);
        } catch (MalformedURLException e){
            e.printStackTrace();
        }
        if(id != -1) {
            Toast.makeText(getApplicationContext(), "Knjiga je uspjesno dodana!", Toast.LENGTH_SHORT).show();
        }
        else if (id == -1){
            Toast.makeText(getApplicationContext(), "Greska!", Toast.LENGTH_SHORT).show();
        }
        Boolean test = true;
        for(int i = 0; i < autors.size(); i++){
            if(autors.get(i).getImeAutora().equals(knjiga.getPisac())){
                autors.get(i).setBrojKnjiga(autors.get(i).getBrojKnjiga() + 1);
                test = false;
            }
            if(!test)break;
        }
        if(test)
            autors.add(new Autor(knjiga.getPisac(), 1));
        fragmentManager.popBackStack();
    }

    @Override
    public void dodavanjeKnjigeOnline(){
        //Bundle bundle=new Bundle();
        //bundle.putStringArrayList("kategorije", unosi);
        //bundle.putSerializable("knjige", knjige);
        //bundle.putSerializable("autori", autors);
        FragmentOnline fragmentOnline=new FragmentOnline();
        //fragmentOnline.setArguments(bundle);
        fragmentManager.beginTransaction().replace(R.id.frame1, fragmentOnline, "fragmentOnline").addToBackStack("fragmentOnline").commit();
    }



    @Override
    public  void povratakOnline(){fragmentManager.popBackStack();}

}
