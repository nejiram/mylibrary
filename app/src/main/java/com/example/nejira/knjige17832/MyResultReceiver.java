package com.example.nejira.knjige17832;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class MyResultReceiver extends ResultReceiver {

    private Receiver mReceiver;

    public void setmReceiver(Receiver mReceiver) {
        this.mReceiver = mReceiver;
    }

    public MyResultReceiver(Handler handler) {
        super(handler);
    }

    public interface Receiver{
        public void onReceiveResult(int resultCode, Bundle resultData);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData){
        if(mReceiver!=null){
            mReceiver.onReceiveResult(resultCode, resultData);
        }
    }
}
