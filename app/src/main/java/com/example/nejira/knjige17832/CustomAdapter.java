
package com.example.nejira.knjige17832;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * Created by Nejira on 30.03.2018..
 */

public class CustomAdapter extends ArrayAdapter{

    int resource;
    Context context;

    public CustomAdapter(@NonNull Context context, int resource, @NonNull List objects) {
        super(context, resource, objects);
        this.resource = resource;
        this.context=context;
    }

    public View getView(int position, View convertView, ViewGroup parent ) {
        View newView;
        if (convertView == null) {
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater layoutInflater = (LayoutInflater)getContext().getSystemService(inflater);
            layoutInflater.inflate(resource, (ViewGroup) newView, true);
        } else {
            newView = (LinearLayout)convertView;
        }
        final Knjiga knjiga = (Knjiga) getItem(position);
        TextView nazivKnjige = newView.findViewById(R.id.eNaziv);
        TextView nazivAutora = newView.findViewById(R.id.eAutor);
        ImageView naslovnaStrana=newView.findViewById(R.id.eNaslovna);
        TextView datumObjavljivanja=newView.findViewById(R.id.eDatumObjavljivanja);
        TextView opis=newView.findViewById(R.id.eOpis);
        TextView brojStranica=newView.findViewById(R.id.eBrojStranica);
        Button preporuci=newView.findViewById(R.id.dPreporuci);
        if(knjiga.isObojen())
            newView.setBackgroundColor(context.getResources().getColor(R.color.obojenaKnjiga));
        if(!knjiga.isJeLiNova()){
            nazivKnjige.setText(knjiga.getNazivKnjige());
            nazivAutora.setText(knjiga.getPisac());
            if(knjiga.getSlikaUrl()!=null){
                try{
                    Bitmap bitmapa= MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), Uri.parse(knjiga.getSlikaUrl()));
                    naslovnaStrana.setImageBitmap(bitmapa);
                }
                catch(IOException e){
                    e.printStackTrace();
                }
            }

        }
        else if(knjiga.isJeLiNova()){
            if(knjiga.getNaziv()!=null) nazivKnjige.setText(knjiga.getNaziv());
            else nazivKnjige.setText("Naziv knjige");
            if(knjiga.getAutori().get(0).getImeiPrezime()!=null) nazivAutora.setText(knjiga.getAutori().get(0).getImeiPrezime());
            else nazivAutora.setText("Autor");
            if(knjiga.getDatumObjavljivanja()!=null) datumObjavljivanja.setText(knjiga.getDatumObjavljivanja());
            else datumObjavljivanja.setText("Datum objavljivanja");
            Log.d("OPIS", knjiga.getOpis()+"OPIS");
            if(knjiga.getOpis()!=null) opis.setText(knjiga.getOpis());
            else opis.setText("Opis");
            if(String.valueOf(knjiga.getBrojStranica())!=null) brojStranica.setText(( String.valueOf(knjiga.getBrojStranica())));
            else brojStranica.setText("Broj stranica");
            if(knjiga.getSlika()!=null) Picasso.with(context).load(knjiga.getSlika().toString()).into(naslovnaStrana);
            else naslovnaStrana.setImageResource(R.drawable.ic_launcher_background);
        }

        preporuci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putParcelable("knjiga", knjiga);
                FragmentPreporuci fragmentPreporuci=new FragmentPreporuci();
                fragmentPreporuci.setArguments(bundle);
                ((FragmentActivity)context).getSupportFragmentManager().beginTransaction().replace(R.id.frame1, fragmentPreporuci, "fragmentPreporuci").addToBackStack("fragmentPreporuci").commit();
            }
        });


        return newView;
    }

}
