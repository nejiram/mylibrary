package com.example.nejira.knjige17832;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.facebook.stetho.inspector.database.SqliteDatabaseDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class BazaOpenHelper extends SQLiteOpenHelper {


    public static final String DATABASE_NAME = "BazaOpenHelper";
    public static final int DATABASE_VERSION = 41;

    //TABELE
    public static final String DATABASE_TABLE_KATEGORIJA = "Kategorija";
    public static final String DATABASE_TABLE_KNJIGA="Knjiga";
    public static final String DATABASE_TABLE_AUTOR = "Autor";
    public static final String DATABASE_TABLE_AUTORSTVO = "Autorstvo";

    //KATEGORIJA
    public static final String KATEGORIJA_ID="_id";
    public static final String KATEGORIJA_NAZIV="naziv";

    //KNJIGA
    public static final String KNJIGA_ID="_id";
    public static final String KNJIGA_NAZIV="naziv";
    public static final String KNJIGA_OPIS="opis";
    public static final String KNJIGA_DATUM_OBJAVLJIVANJA="datumObjavljivanja";
    public static final String KNJIGA_BROJ_STRANICA="brojStranica";
    public static final String KNJIGA_ID_WEB_SERVIS="idWebServis";
    public static final String KNJIGA_ID_KATEGORIJE="idkategorije";
    public static final String KNJIGA_SLIKA="slika";
    public static final String KNJIGA_PREGLEDANA="pregledana";

    //AUTOR
    public static final String AUTOR_ID = "_id";
    public static final String AUTOR_IME = "ime";

    //AUTORSTVO
    public static final String AUTORSTVO_ID = "_id";
    public static final String AUTORSTVO_ID_AUTORA = "idautora";
    public static final String AUTORSTVO_ID_KNJIGE = "idknjige";

    private static final String CREATE_TABLE_KATEGORIJA = "CREATE TABLE " + DATABASE_TABLE_KATEGORIJA + " (" +
    KATEGORIJA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
    KATEGORIJA_NAZIV + " TEXT );";

    private static final String CREATE_TABLE_KNJIGA = "CREATE TABLE " + DATABASE_TABLE_KNJIGA + " (" +
            KNJIGA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            KNJIGA_NAZIV + " TEXT, " +
            KNJIGA_OPIS + " TEXT, " +
            KNJIGA_DATUM_OBJAVLJIVANJA + " TEXT, " +
            KNJIGA_BROJ_STRANICA + " INTEGER, " +
            KNJIGA_ID_WEB_SERVIS + " TEXT, " +
            KNJIGA_ID_KATEGORIJE + " INTEGER, " +
            KNJIGA_SLIKA + " TEXT, " +
            KNJIGA_PREGLEDANA + " INTEGER );";

    private static final String CREATE_TABLE_AUTOR = "CREATE TABLE " + DATABASE_TABLE_AUTOR + " (" +
            AUTOR_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            AUTOR_IME + " TEXT );";

    private static final String CREATE_TABLE_AUTORSTVO = "CREATE TABLE " + DATABASE_TABLE_AUTORSTVO + " (" +
            AUTORSTVO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            AUTORSTVO_ID_AUTORA + " INTEGER, " +
            AUTORSTVO_ID_KNJIGE + " INTEGER );";


    public BazaOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_KATEGORIJA);
        db.execSQL(CREATE_TABLE_KNJIGA);
        db.execSQL(CREATE_TABLE_AUTOR);
        db.execSQL(CREATE_TABLE_AUTORSTVO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_KATEGORIJA);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_KNJIGA);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_AUTOR);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_AUTORSTVO);

        onCreate(db);
    }

    public long dodajKategoriju(String naziv){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        Cursor cursor = db.query(BazaOpenHelper.DATABASE_TABLE_KATEGORIJA, new String[]{KATEGORIJA_ID, KATEGORIJA_NAZIV}, null, null, null, null, null);
        if(cursor.getCount()>0){
            while (cursor.moveToNext()){
                if(cursor.getString(cursor.getColumnIndex(KATEGORIJA_NAZIV)).equals(naziv)) return -1;
            }
        }
        cursor.close();

        values.put(KATEGORIJA_NAZIV, naziv);
        long id = db.insert(BazaOpenHelper.DATABASE_TABLE_KATEGORIJA, null, values);
        db.close();
        return id;
    }

    public ArrayList<String> dajKategorije(){
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<String> kategorije = new ArrayList<>();
        Cursor cursor = db.query(BazaOpenHelper.DATABASE_TABLE_KATEGORIJA, new String[]{KATEGORIJA_ID, KATEGORIJA_NAZIV}, null, null, null, null, null);
        if(cursor.getCount()>0){
            while (cursor.moveToNext()){
                kategorije.add(cursor.getString(cursor.getColumnIndex(KATEGORIJA_NAZIV)));
            }
        }
        db.close();
        return kategorije;
    }

    public long dodajKnjigu(Knjiga knjiga) throws MalformedURLException {
        SQLiteDatabase db = this.getWritableDatabase();
        long id = -1;
        if(knjiga.isJeLiNova()){
            String[] pom = new String[] {KNJIGA_ID, KNJIGA_NAZIV, KNJIGA_OPIS, KNJIGA_BROJ_STRANICA, KNJIGA_DATUM_OBJAVLJIVANJA, KNJIGA_ID_WEB_SERVIS, KNJIGA_ID_KATEGORIJE, KNJIGA_SLIKA, KNJIGA_PREGLEDANA };
            Cursor cursor = db.query(BazaOpenHelper.DATABASE_TABLE_KNJIGA, pom, null, null, null, null, null);
            if(cursor.getCount()>0){
                while (cursor.moveToNext()){
                    if(cursor.getString(cursor.getColumnIndex(KNJIGA_NAZIV)).equals(knjiga.getNaziv())) return -1;
                }
            }
            cursor.close();

            ContentValues values = new ContentValues();
            values.put(KNJIGA_NAZIV, knjiga.getNaziv());
            values.put(KNJIGA_OPIS, knjiga.getOpis());
            values.put(KNJIGA_BROJ_STRANICA, knjiga.getBrojStranica());
            values.put(KNJIGA_DATUM_OBJAVLJIVANJA, knjiga.getDatumObjavljivanja());
            values.put(KNJIGA_ID_WEB_SERVIS, knjiga.getId());
            int idKategorije = 0;
            cursor = db.query(BazaOpenHelper.DATABASE_TABLE_KATEGORIJA, new String[]{KATEGORIJA_ID, KATEGORIJA_NAZIV}, null, null, null, null, null);
            if(cursor.getCount()>0){
                while (cursor.moveToNext()){
                    if(cursor.getString(cursor.getColumnIndex(KATEGORIJA_NAZIV)).equals(knjiga.getKategorija())) idKategorije = cursor.getInt(cursor.getColumnIndex(KATEGORIJA_ID));
                }
            }
            cursor.close();
            values.put(KNJIGA_ID_KATEGORIJE, idKategorije);
            if(knjiga.getSlika() != null) values.put(KNJIGA_SLIKA, knjiga.getSlika().toString());
            else{
                values.put(KNJIGA_SLIKA, String.valueOf(R.drawable.ic_launcher_background));
            }
            if(knjiga.isObojen()) values.put(KNJIGA_PREGLEDANA, 1);
            else values.put(KNJIGA_PREGLEDANA, 0);
            Log.d("PRIJE", "INSERTA");
            id = db.insert(BazaOpenHelper.DATABASE_TABLE_KNJIGA, null,values);
            Log.d("IDDDDD", id+"");
            //ubaci u autore i autorstvo
            ArrayList<Autor> autori = knjiga.getAutori();
            for(int i = 0; i <autori.size(); i++){
                cursor = db.query(BazaOpenHelper.DATABASE_TABLE_AUTOR, new String[]{AUTOR_ID, AUTOR_IME}, null, null, null, null, null);
                boolean postoji = false;
                if(cursor.getCount()>0){
                    while (cursor.moveToNext()){
                        if(cursor.getString(cursor.getColumnIndex(AUTOR_IME)).equals(autori.get(i).getImeiPrezime())){
                            postoji = true;
                            values = new ContentValues();
                            values.put(AUTORSTVO_ID_AUTORA, cursor.getInt(cursor.getColumnIndex(AUTOR_ID)));
                            values.put(AUTORSTVO_ID_KNJIGE, id);
                            db.insert(BazaOpenHelper.DATABASE_TABLE_AUTORSTVO, null, values);
                        }
                    }
                }
                cursor.close();
                if (!postoji){
                    values = new ContentValues();
                    values.put(AUTOR_IME, autori.get(i).getImeiPrezime());
                    db.insert(DATABASE_TABLE_AUTOR, null, values);
                    cursor = db.query(BazaOpenHelper.DATABASE_TABLE_AUTOR, new String[]{AUTOR_ID, AUTOR_IME}, null, null, null, null, null);
                    if(cursor.getCount()>0){
                        while (cursor.moveToNext()){
                            if(cursor.getString(cursor.getColumnIndex(AUTOR_IME)).equals(autori.get(i).getImeiPrezime())){
                                values = new ContentValues();
                                values.put(AUTORSTVO_ID_AUTORA, cursor.getInt(cursor.getColumnIndex(AUTOR_ID)));
                                values.put(AUTORSTVO_ID_KNJIGE, id);
                                db.insert(BazaOpenHelper.DATABASE_TABLE_AUTORSTVO, null, values);
                            }
                        }
                    }
                }
            }
            cursor.close();
        }else{
            //ako je dodana po starom
            String[] pom = new String[] {KNJIGA_ID, KNJIGA_NAZIV, KNJIGA_OPIS, KNJIGA_BROJ_STRANICA, KNJIGA_DATUM_OBJAVLJIVANJA, KNJIGA_ID_WEB_SERVIS, KNJIGA_ID_KATEGORIJE, KNJIGA_SLIKA, KNJIGA_PREGLEDANA };
            Cursor cursor = db.query(BazaOpenHelper.DATABASE_TABLE_KNJIGA, pom, null, null, null, null, null);
            if(cursor.getCount()>0){
                while (cursor.moveToNext()){
                    if(cursor.getString(cursor.getColumnIndex(KNJIGA_NAZIV)).equals(knjiga.getNazivKnjige())) return -1;
                }
            }
            cursor.close();
            ContentValues values = new ContentValues();
            values.put(KNJIGA_NAZIV, knjiga.getNazivKnjige());
            //kategorija
            int idKategorije = 0;
            cursor = db.query(BazaOpenHelper.DATABASE_TABLE_KATEGORIJA, new String[]{KATEGORIJA_ID, KATEGORIJA_NAZIV}, null, null, null, null, null);
            if(cursor.getCount()>0){
                while (cursor.moveToNext()){
                    if(cursor.getString(cursor.getColumnIndex(KATEGORIJA_NAZIV)).equals(knjiga.getKategorija())) idKategorije = cursor.getInt(cursor.getColumnIndex(KATEGORIJA_ID));
                }
            }
            cursor.close();
            values.put(KNJIGA_ID_KATEGORIJE, idKategorije);
            values.put(KNJIGA_SLIKA, String.valueOf(R.drawable.ic_launcher_background));
            if(knjiga.isObojen()) values.put(KNJIGA_PREGLEDANA, 1);
            else values.put(KNJIGA_PREGLEDANA, 0);
            id = db.insert(BazaOpenHelper.DATABASE_TABLE_KNJIGA, null, values);
            //autori i autorstvo
            cursor = db.query(BazaOpenHelper.DATABASE_TABLE_AUTOR, new String[]{AUTOR_ID, AUTOR_IME}, null, null, null, null, null);
            boolean postoji = false;
            if(cursor.getCount()>0){
                while (cursor.moveToNext()){
                    if(cursor.getString(cursor.getColumnIndex(AUTOR_IME)).equals(knjiga.getPisac())){
                        postoji = true;
                        values = new ContentValues();
                        values.put(AUTORSTVO_ID_AUTORA, cursor.getInt(cursor.getColumnIndex(AUTOR_ID)));
                        values.put(AUTORSTVO_ID_KNJIGE, id);
                        db.insert(BazaOpenHelper.DATABASE_TABLE_AUTORSTVO, null, values);
                    }
                }
            }
            cursor.close();
            if(!postoji){
                values = new ContentValues();
                values.put(AUTOR_IME, knjiga.getPisac());
                db.insert(DATABASE_TABLE_AUTOR, null, values);
                cursor = db.query(BazaOpenHelper.DATABASE_TABLE_AUTOR, new String []{AUTOR_ID, AUTOR_IME}, null, null, null, null, null);
                if(cursor.getCount()>0){
                    while (cursor.moveToNext()){
                        if(cursor.getString(cursor.getColumnIndex(AUTOR_IME)).equals(knjiga.getPisac())){
                            values = new ContentValues();
                            values.put(AUTORSTVO_ID_AUTORA, cursor.getInt(cursor.getColumnIndex(AUTOR_ID)));
                            values.put(AUTORSTVO_ID_KNJIGE, id);
                            db.insert(BazaOpenHelper.DATABASE_TABLE_AUTORSTVO, null, values);
                        }
                    }
                }
                cursor.close();
            }
        }
        db.close();
        return id;
    }

    public ArrayList<Knjiga> dajKnjige() {
        ArrayList<Knjiga> knjige = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        String[] pom = new String[] {KNJIGA_ID, KNJIGA_NAZIV, KNJIGA_OPIS, KNJIGA_BROJ_STRANICA, KNJIGA_DATUM_OBJAVLJIVANJA, KNJIGA_ID_WEB_SERVIS, KNJIGA_ID_KATEGORIJE, KNJIGA_SLIKA, KNJIGA_PREGLEDANA };
        Cursor cursor = db.query(BazaOpenHelper.DATABASE_TABLE_KNJIGA, pom, null, null, null, null, null);
        if (cursor.getCount() > 0){
            while (cursor.moveToNext()){
                if (cursor.getString(cursor.getColumnIndex(KNJIGA_ID_WEB_SERVIS)) != null){
                    //ovo je nova knjiga
                    Knjiga k = new Knjiga();
                    k.setNaziv(cursor.getString(cursor.getColumnIndex(KNJIGA_NAZIV)));
                    k.setId(cursor.getString(cursor.getColumnIndex(KNJIGA_ID_WEB_SERVIS)));
                    try {
                        k.setSlika(new URL(cursor.getString(cursor.getColumnIndex(KNJIGA_SLIKA))));
                    }catch (MalformedURLException e){
                        e.printStackTrace();
                    }
                    k.setDatumObjavljivanja(cursor.getString(cursor.getColumnIndex(KNJIGA_DATUM_OBJAVLJIVANJA)));
                    k.setOpis(cursor.getString(cursor.getColumnIndex(KNJIGA_OPIS)));
                    if(cursor.getInt(cursor.getColumnIndex(KNJIGA_PREGLEDANA)) == 1) k.setObojen(true);
                    else k.setObojen(false);
                    k.setBrojStranica(cursor.getInt(cursor.getColumnIndex(KNJIGA_BROJ_STRANICA)));

                    //kategorija
                    Cursor cursor1 = db.query(BazaOpenHelper.DATABASE_TABLE_KATEGORIJA, new String[]{KATEGORIJA_ID, KATEGORIJA_NAZIV}, null, null, null, null,null);
                    if(cursor1.getCount()>0){
                        while (cursor1.moveToNext()){
                            if(cursor1.getInt(cursor1.getColumnIndex(KATEGORIJA_ID)) == cursor.getInt(cursor.getColumnIndex(KNJIGA_ID_KATEGORIJE))) k.setKategorija(cursor1.getString(cursor1.getColumnIndex(KATEGORIJA_NAZIV)));
                        }
                    }
                    cursor1.close();

                    //autori
                    cursor1 = db.query(DATABASE_TABLE_AUTOR, new String[]{AUTOR_ID, AUTOR_IME}, null, null, null, null, null);
                    Cursor cursor2 = db.query(DATABASE_TABLE_AUTORSTVO, new String[]{AUTORSTVO_ID_AUTORA, AUTORSTVO_ID_KNJIGE}, null, null, null, null, null );
                    if(cursor2.getCount()>0) {
                        while (cursor2.moveToNext()) {
                            if (cursor.getInt(cursor.getColumnIndex(KNJIGA_ID)) == cursor2.getInt(cursor2.getColumnIndex(AUTORSTVO_ID_KNJIGE))) {
                                if(cursor1.getCount()>0) {
                                    while (cursor1.moveToNext()) {
                                        if (cursor1.getInt(cursor1.getColumnIndex(AUTOR_ID)) == cursor2.getInt(cursor2.getColumnIndex(AUTORSTVO_ID_AUTORA)))
                                            k.dodajAutora(cursor1.getString(cursor1.getColumnIndex(AUTOR_IME)));
                                    }
                                    cursor1.moveToFirst();
                                }
                            }
                        }
                    }
                    cursor1.close();
                    cursor2.close();
                    k.setJeLiNova(true);
                    knjige.add(k);
                }
                else{
                    //stara knjiga
                    Knjiga k = new Knjiga();
                    k.setNazivKnjige(cursor.getString(cursor.getColumnIndex(KNJIGA_NAZIV)));
                    k.setSlikaUrl(cursor.getString(cursor.getColumnIndex(KNJIGA_SLIKA)));
                    if(cursor.getInt(cursor.getColumnIndex(KNJIGA_PREGLEDANA)) == 1) k.setObojen(true);
                    else k.setObojen(false);
                    //kategorije
                    Cursor cursor1 = db.query(BazaOpenHelper.DATABASE_TABLE_KATEGORIJA, new String[]{KATEGORIJA_ID, KATEGORIJA_NAZIV}, null, null, null, null,null);
                    if(cursor1.getCount()>0){
                        while (cursor1.moveToNext()){
                            if(cursor1.getInt(cursor1.getColumnIndex(KATEGORIJA_ID)) == cursor.getInt(cursor.getColumnIndex(KNJIGA_ID_KATEGORIJE))) k.setKategorija(cursor1.getString(cursor1.getColumnIndex(KATEGORIJA_NAZIV)));
                        }
                    }
                    cursor1.close();
                    //autori
                    cursor1 = db.query(DATABASE_TABLE_AUTOR, new String[]{AUTOR_ID, AUTOR_IME}, null, null, null, null, null);
                    Cursor cursor2 = db.query(DATABASE_TABLE_AUTORSTVO, new String[]{AUTORSTVO_ID_AUTORA, AUTORSTVO_ID_KNJIGE}, null, null, null, null, null );
                    if(cursor2.getCount()>0) {
                        while (cursor2.moveToNext()) {
                            if (cursor.getInt(cursor.getColumnIndex(KNJIGA_ID)) == cursor2.getInt(cursor2.getColumnIndex(AUTORSTVO_ID_KNJIGE))) {
                                if(cursor1.getCount()>0) {
                                    while (cursor1.moveToNext()) {
                                        if (cursor1.getInt(cursor1.getColumnIndex(AUTOR_ID)) == cursor2.getInt(cursor2.getColumnIndex(AUTORSTVO_ID_AUTORA)))
                                            k.setPisac(cursor1.getString(cursor1.getColumnIndex(AUTOR_IME)));
                                    }
                                    cursor1.moveToFirst();
                                }
                            }
                        }
                    }
                    cursor1.close();
                    cursor2.close();
                    k.setJeLiNova(false);
                    knjige.add(k);
                }
            }
        }
        return knjige;
    }

    public ArrayList<Knjiga> knjigeKategorije(long idKategorije){
        ArrayList<Knjiga> knjige = new ArrayList<>();
        ArrayList<Knjiga> trenutneKnjige = this.dajKnjige();
        for(int i = 0; i < trenutneKnjige.size(); i++){
            SQLiteDatabase db=this.getWritableDatabase();
            Cursor cursor = db.query(BazaOpenHelper.DATABASE_TABLE_KATEGORIJA, new String[]{KATEGORIJA_ID, KATEGORIJA_NAZIV}, null, null ,null, null, null);
            String kategorija = "";
            while (cursor.moveToNext()){
                if(cursor.getInt(cursor.getColumnIndex(KATEGORIJA_ID))==(int)idKategorije) kategorija = cursor.getString(cursor.getColumnIndex(KATEGORIJA_NAZIV));
            }
            if(!kategorija.equals("")){
                if(trenutneKnjige.get(i).getKategorija().equals(kategorija)) knjige.add(trenutneKnjige.get(i));
            }
            cursor.close();
        }
        return knjige;
    }

    public long dajIdKategorije(String kategorija){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(BazaOpenHelper.DATABASE_TABLE_KATEGORIJA, new String[]{KATEGORIJA_ID, KATEGORIJA_NAZIV}, null, null, null, null, null);
        long id = -1;
        while (cursor.moveToNext()){
            if(cursor.getString(cursor.getColumnIndex(KATEGORIJA_NAZIV)).equals(kategorija)) id = cursor.getLong(cursor.getColumnIndex(KATEGORIJA_ID));
        }
        db.close();
        return id;
    }

    public long dajAutora(String ime){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(BazaOpenHelper.DATABASE_TABLE_AUTOR, new String[]{AUTOR_ID, AUTOR_IME}, null, null, null, null, null);
        long id = -1;
        while (cursor.moveToNext()){
            if(cursor.getString(cursor.getColumnIndex(AUTOR_IME)).equals(ime)) id = cursor.getLong(cursor.getColumnIndex(AUTOR_ID));
        }
        cursor.close();
        db.close();
        return id;
    }

    ArrayList<Knjiga> knjigeAutora(long idAutora) {
        ArrayList<Knjiga> knjige = new ArrayList<>();
        ArrayList<Knjiga> trenutneKnjige = this.dajKnjige();
        SQLiteDatabase db = this.getWritableDatabase();
        String[] pom = new String[] {KNJIGA_ID, KNJIGA_NAZIV, KNJIGA_OPIS, KNJIGA_BROJ_STRANICA, KNJIGA_DATUM_OBJAVLJIVANJA, KNJIGA_ID_WEB_SERVIS, KNJIGA_ID_KATEGORIJE, KNJIGA_SLIKA, KNJIGA_PREGLEDANA };
        Cursor cursor = db.query(BazaOpenHelper.DATABASE_TABLE_KNJIGA, pom, null, null, null, null, null);
        Cursor cursor1 = db.query(BazaOpenHelper.DATABASE_TABLE_AUTORSTVO, new String[]{AUTORSTVO_ID_AUTORA, AUTORSTVO_ID_KNJIGE}, null, null, null, null, null);
        if(cursor1.getCount()>0) {
            while (cursor1.moveToNext()) {
                if (cursor1.getInt(cursor1.getColumnIndex(AUTORSTVO_ID_AUTORA)) == idAutora) {
                    if(cursor.getCount()>0) {
                        while (cursor.moveToNext()) {
                            if (cursor1.getInt(cursor1.getColumnIndex(AUTORSTVO_ID_KNJIGE)) == cursor.getInt(cursor.getColumnIndex(KNJIGA_ID))) {
                                for (int i = 0; i < trenutneKnjige.size(); i++) {
                                    if (trenutneKnjige.get(i).isJeLiNova()) {
                                        if (trenutneKnjige.get(i).getNaziv().equals(cursor.getString(cursor.getColumnIndex(KNJIGA_NAZIV))))
                                            knjige.add(new Knjiga(trenutneKnjige.get(i)));
                                    } else {
                                        if (trenutneKnjige.get(i).getNazivKnjige().equals(cursor.getString(cursor.getColumnIndex(KNJIGA_NAZIV))))
                                            knjige.add(new Knjiga(trenutneKnjige.get(i)));
                                    }
                                }
                            }
                        }
                        cursor.moveToFirst();
                    }
                }
            }
        }
        db.close();
        return knjige;
    }

    ArrayList<Autor> dajListuAutora(){
        SQLiteDatabase db = this.getWritableDatabase();

        ArrayList<Autor> rezultat = new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT * FROM " + DATABASE_TABLE_AUTOR , null);
        if(cursor.getCount()>0){
            while(cursor.moveToNext()){
                Autor a = new Autor();
                a.setImeAutora(cursor.getString(1));
                int brojac = 0;
                Cursor cursor1 = db.rawQuery("SELECT * FROM " + DATABASE_TABLE_AUTORSTVO + " WHERE " + AUTORSTVO_ID_AUTORA + " = ?",new String[]{String.valueOf(cursor.getLong(0))});
                if(cursor1.getCount() > 0){
                    while(cursor1.moveToNext()){
                        brojac++;
                    }
                }
                a.setBrojKnjiga(brojac);
                rezultat.add(a);
                cursor1.close();
            }
        }
        cursor.close();


        db.close();
        return rezultat;
    }

    public void oboji(String naziv, boolean oboji){
        SQLiteDatabase db = this.getWritableDatabase();
        String[] pom = new String[] {KNJIGA_ID, KNJIGA_NAZIV, KNJIGA_OPIS, KNJIGA_BROJ_STRANICA, KNJIGA_DATUM_OBJAVLJIVANJA, KNJIGA_ID_WEB_SERVIS, KNJIGA_ID_KATEGORIJE, KNJIGA_SLIKA, KNJIGA_PREGLEDANA };
        Cursor cursor = db.query(BazaOpenHelper.DATABASE_TABLE_KNJIGA, pom, null, null, null, null, null);
        if(cursor.getCount()>0){
            while (cursor.moveToNext()){
                if(cursor.getString(cursor.getColumnIndex(KNJIGA_NAZIV)).equals(naziv)){
                    if(cursor.getString(cursor.getColumnIndex(KNJIGA_ID_WEB_SERVIS)) != null){
                        ContentValues values = new ContentValues();
                        values.put(KNJIGA_NAZIV, cursor.getString(cursor.getColumnIndex(KNJIGA_NAZIV)));
                        values.put(KNJIGA_OPIS, cursor.getString(cursor.getColumnIndex(KNJIGA_OPIS)));
                        values.put(KNJIGA_DATUM_OBJAVLJIVANJA, cursor.getString(cursor.getColumnIndex(KNJIGA_DATUM_OBJAVLJIVANJA)));
                        values.put(KNJIGA_BROJ_STRANICA, cursor.getInt(cursor.getColumnIndex(KNJIGA_BROJ_STRANICA)));
                        values.put(KNJIGA_ID_WEB_SERVIS, cursor.getString(cursor.getColumnIndex(KNJIGA_ID_WEB_SERVIS)));
                        values.put(KNJIGA_ID_KATEGORIJE, cursor.getInt(cursor.getColumnIndex(KNJIGA_ID_KATEGORIJE)));
                        values.put(KNJIGA_SLIKA, cursor.getString(cursor.getColumnIndex(KNJIGA_SLIKA)));
                        if(oboji) values.put(KNJIGA_PREGLEDANA, 1);
                        else values.put(KNJIGA_PREGLEDANA,0);
                        int id = cursor.getInt(cursor.getColumnIndex(KNJIGA_ID));
                        db.update(BazaOpenHelper.DATABASE_TABLE_KNJIGA, values ,"_id="+id, null);
                    }else{
                        ContentValues values = new ContentValues();
                        values.put(KNJIGA_NAZIV, cursor.getString(cursor.getColumnIndex(KNJIGA_NAZIV)));
                        values.put(KNJIGA_ID_KATEGORIJE, cursor.getInt(cursor.getColumnIndex(KNJIGA_ID_KATEGORIJE)));
                        values.put(KNJIGA_SLIKA, cursor.getString(cursor.getColumnIndex(KNJIGA_SLIKA)));
                        if(oboji) values.put(KNJIGA_PREGLEDANA, 1);
                        else values.put(KNJIGA_PREGLEDANA,0);
                        int id = cursor.getInt(cursor.getColumnIndex(KNJIGA_ID));
                        db.update(BazaOpenHelper.DATABASE_TABLE_KNJIGA, values ,"_id="+id, null);
                    }
                }
            }
        }
        cursor.close();
        db.close();
    }

}
