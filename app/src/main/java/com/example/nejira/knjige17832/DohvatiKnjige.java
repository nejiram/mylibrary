package com.example.nejira.knjige17832;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class DohvatiKnjige extends AsyncTask<String, Integer, Void> {

    private ArrayList<Knjiga> rez=new ArrayList<>();

    public interface IDohvatiKnjigeDone{
        public void onDohvatiDone(ArrayList<Knjiga> rez);
    }

    private IDohvatiKnjigeDone pozivatelj;

    public DohvatiKnjige(IDohvatiKnjigeDone p){
        pozivatelj=p;
    }

    @Override
    protected Void doInBackground(String... params) {

        for(int k=0; k<params.length; k++) {

            String query = null;

            try {
                query = URLEncoder.encode(params[k], "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            String url1 = "https://www.googleapis.com/books/v1/volumes?q=intitle:" + query + "&maxResults=5";
            try {
                URL url = new URL(url1);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    String rezultat = convertStreamToString(in);
                    JSONObject jo = new JSONObject(rezultat);
                    JSONArray items = jo.getJSONArray("items");

                    String name = "", id = "", description = "", publishedDate = "", picture="";
                    int pages = 0;
                    ArrayList<Autor> listdata;
                    URL image;

                    for (int i = 0; i < items.length(); i++) {
                        JSONObject book = items.getJSONObject(i);
                        if (book.has("id")) id = book.getString("id");
                        JSONObject vInfo = book.getJSONObject("volumeInfo");
                        if (vInfo.has("title")) name = vInfo.getString("title");

                        listdata = new ArrayList<Autor>();
                        if (vInfo.has("authors")){
                            JSONArray authors = vInfo.getJSONArray("authors");
                            for (int j = 0; j < authors.length(); j++) {
                                if(authors.getString(j).equals("")) listdata.add(new Autor("Ime i prezime"));
                                else listdata.add(new Autor(authors.getString(j)));
                            }
                        }
                        else listdata.add(new Autor("Ime i prezime"));

                        if (vInfo.has("description")) description = vInfo.getString("description");
                        else  description = "Opis";
                        //if(vInfo.has("thumbnail")) image = (URL) vInfo.get("thumbnail");
                        if(vInfo.has("imageLinks") && vInfo.getJSONObject("imageLinks").has("thumbnail")) {
                            picture = vInfo.getJSONObject("imageLinks").getString("thumbnail");
                            image=new URL(picture);
                        }
                        else image=null;
                        if (vInfo.has("publishedDate"))  publishedDate = vInfo.getString("publishedDate");
                        else publishedDate = "Datum objavljivanja";
                        if (vInfo.has("pageCount")) pages = vInfo.getInt("pageCount");
                        rez.add(new Knjiga(id, name, listdata, description, publishedDate, image, pages));
                    }
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private String convertStreamToString(InputStream in) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                in.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        pozivatelj.onDohvatiDone(rez);
    }

}
